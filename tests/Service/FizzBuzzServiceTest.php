<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Service\FizzBuzzService;
use PHPUnit\Framework\TestCase;

/**
 * Class FizzBuzzServiceTest
 * @package App\Tests\Service
 */
class FizzBuzzServiceTest extends TestCase
{
    /**
     * @dataProvider dataPrintFizzBuzzWithinOneToHundred
     * @param $expected
     */
    public function testPrintFizzBuzzWithinOneToHundred(string $expected)
    {
        $service = new FizzBuzzService();
        $resultAsArray = $service->getFizzBuzzWithinOneToHundred();
        $resultAsString = implode(PHP_EOL, $resultAsArray);
        $this->assertStringContainsString($expected, $resultAsString);

    }

    /**
     * @return array
     */
    public function dataPrintFizzBuzzWithinOneToHundred(): array
    {
        $expectedCase1 = "Fizz" . PHP_EOL . "94" . PHP_EOL . "Buzz" . PHP_EOL . "Fizz" . PHP_EOL . "97" . PHP_EOL . "98";
        $expectedCase2 = "1" . PHP_EOL . "2" . PHP_EOL . "Fizz" . PHP_EOL . "4" . PHP_EOL . "Buzz" . PHP_EOL . "Fizz";
        $expectedCase3 = "8" . PHP_EOL . "Fizz" . PHP_EOL . "Buzz" . PHP_EOL . "11" . PHP_EOL . "Fizz" . PHP_EOL . "13". PHP_EOL . "14". PHP_EOL . "FizzBuzz";

        return [
            "Test Expect Buzz If Number Is Multiply of Five" => [$expectedCase1],
            "Test Expect Fizz If Number Is Multiply Three" => [$expectedCase2],
            "Test Expect FizzBuzz If Number Is Multiply Three And Five" => [$expectedCase3],
        ];
    }

    public function testCanCreate(): void
    {
        $fizzBuzzService = new FizzBuzzService();
        $this->assertInstanceOf(FizzBuzzService::class, $fizzBuzzService);
    }
}