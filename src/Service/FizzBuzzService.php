<?php

declare(strict_types=1);

namespace App\Service;

/**
 * Class FizzBuzzService
 * @package App\Service
 */
class FizzBuzzService
{
    private const LABEL_FIZZ = "Fizz";
    private const LABEL_BUZZ = "Buzz";

    /**
     * @return array
     */
    public function getFizzBuzzWithinOneToHundred(): array
    {
        $arrayResult = [];
        for ($number = 1; $number < 101; $number++) {
            if (
                $this->isMultipleOfFive($number) &&
                $this->isMultipleOfThree($number)
            ) {
                $arrayResult[] = self::LABEL_FIZZ . self::LABEL_BUZZ;
            } elseif ($this->isMultipleOfThree($number)) {
                $arrayResult[] = self::LABEL_FIZZ;
            } elseif ($this->isMultipleOfFive($number)) {
                $arrayResult[] = self::LABEL_BUZZ;
            } else {
                $arrayResult[] = $number;
            }
        }

        return $arrayResult;
    }

    /**
     * @param int $number
     * @return bool
     */
    private function isMultipleOfThree(int $number): bool
    {
        return $number % 3 === 0;
    }

    /**
     * @param int $number
     * @return bool
     */
    private function isMultipleOfFive(int $number): bool
    {
        return $number % 5 === 0;
    }
}