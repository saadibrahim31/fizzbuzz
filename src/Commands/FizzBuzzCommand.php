<?php

declare(strict_types=1);

namespace App\Commands;

use App\Service\FizzBuzzService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class FizzBuzzCommand
 * @package App\Commands
 */
class FizzBuzzCommand extends Command
{
    /**
     * @var FizzBuzzService
     */
    private $fizzBuzzService;

    /**
     * @param FizzBuzzService $fizzBuzzService
     */
    public function __construct(FizzBuzzService $fizzBuzzService)
    {
        parent::__construct();
        $this->fizzBuzzService = $fizzBuzzService;
    }

    protected function configure()
    {
        $this->setName("print-fizzbuzz")
            ->getDescription("Print fizz buzz within numbers from 1 - 100" . PHP_EOL
                . 'example:' . PHP_EOL
                . ' php bin/console/fizzbuzz print-fizzbuzz' . PHP_EOL
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $result = $this->fizzBuzzService->getFizzBuzzWithinOneToHundred();
            $output->write(implode(PHP_EOL, $result));
        } catch (\Throwable $e) {
            $output->write(json_encode(['success' => false, 'reason' => $e->getMessage()]));
        }

        return Command::SUCCESS;
    }
}