# FizzBuzz

Write a program that prints the numbers from 1 to 100. But for multiples of three print Fizz instead of the number and for the multiples of five print Buzz. For numbers which are multiples of both three and five print FizzBuzz.

### Technology
You can write plain-php code or use a framework for this challenge.

Please use PHP 7.4+, strict typing and object oriented programming.

Also provide some Unit-Tests to verify the sample output.
### Sample-Output
```
1
2
Fizz
4
Buzz
Fizz
7
8
Fizz
Buzz
11
Fizz
13
14
FizzBuzz
16
17
Fizz
19
Buzz
... etc up to 100
```
### Requirements
install
PHP7.4+, composer

###How To
1- clone the project locally

2- run composer inside root folder
```
composer install
```

3- from command line run application
```
php bin/console/fizzbuzz print-fizzbuzz
```
php bin/console/fizzbuzz print-fizzbuzz
response
```json5
1
2
Fizz
4
Buzz
Fizz
7
8
Fizz
Buzz
11
Fizz
13
14
FizzBuzz
16
17
Fizz
19
Buzz
Fizz
22
23
Fizz
Buzz
26
Fizz
28
29
FizzBuzz
31
32
Fizz
34
Buzz
Fizz
37
38
Fizz
Buzz
41
Fizz
43
44
FizzBuzz
46
47
Fizz
49
Buzz
Fizz
52
53
Fizz
Buzz
56
Fizz
58
59
FizzBuzz
61
62
Fizz
64
Buzz
Fizz
67
68
Fizz
Buzz
71
Fizz
73
74
FizzBuzz
76
77
Fizz
79
Buzz
Fizz
82
83
Fizz
Buzz
86
Fizz
88
89
FizzBuzz
91
92
Fizz
94
Buzz
Fizz
97
98
Fizz
```


### To run tests:

4- tests
```
./vendor/bin/phpunit tests
```

```
PHPUnit 9.5.4 by Sebastian Bergmann and contributors.

.....                                                               5 / 5 (100%)

Time: 00:00.010, Memory: 6.00 MB

OK (5 tests, 5 assertions)
```